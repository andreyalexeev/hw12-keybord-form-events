"use strict";

const buttons = document.querySelectorAll('.btn');
let lastBlueButton = null;

document.addEventListener('keydown', function(event) {
    const key = event.key.toUpperCase();
    const button = Array.from(buttons).find(btn => btn.textContent === key);

    if (button) {
        if (lastBlueButton) {
                lastBlueButton.style.backgroundColor = '#000000';
        }
    button.style.backgroundColor = '#0000FF';
    lastBlueButton = button;
    }
});

